#!/usr/bin/env python

# Major thanks to the Acorn Pooley and Mike Lautman for the helpful tutorial!
# This code is based off their MoveIt tutorial for moving a Panda arm using ROS.

# Python 2/3 compatibility imports
from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi # Sets tau value to base joint rotations off of 

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

def all_close(goal, actual, tolerance):
    """
    Convenience method for testing if the values in two lists are within a tolerance of each other.
    For Pose and PoseStamped inputs, the angle between the two quaternions is compared (the angle
    between the identical orientations q and -q is calculated correctly).
    @param: goal       A list of floats, a Pose or a PoseStamped
    @param: actual     A list of floats, a Pose or a PoseStamped
    @param: tolerance  A float
    @returns: bool
    """
    if type(goal) is list:
        for index in range(len(goal)):
            if abs(actual[index] - goal[index]) > tolerance:
                return False

    elif type(goal) is geometry_msgs.msg.PoseStamped:
        return all_close(goal.pose, actual.pose, tolerance)

    elif type(goal) is geometry_msgs.msg.Pose:
        x0, y0, z0, qx0, qy0, qz0, qw0 = pose_to_list(actual)
        x1, y1, z1, qx1, qy1, qz1, qw1 = pose_to_list(goal)
        ## Euclidean distance
        d = dist((x1, y1, z1), (x0, y0, z0))
        ## phi = angle between orientations
        cos_phi_half = fabs(qx0 * qx1 + qy0 * qy1 + qz0 * qz1 + qw0 * qw1)
        return d <= tolerance and cos_phi_half >= cos(tolerance / 2.0)

    return True


class initialWriter(object):
    # Defines the robotic movement as an object under this class

    def __init__(self):
        super(initialWriter, self).__init__()

        # Initializes `moveit_commander`_ and a `rospy`_ node:
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

        # Instantiate a `RobotCommander`_ object
        # Gives kinematic and joint info for robot
        robot = moveit_commander.RobotCommander()

        # Instantiate a `PlanningSceneInterface`_ object
        # Provides an understanding and information of the robot's world
        scene = moveit_commander.PlanningSceneInterface()

        # Instantiate a `MoveGroupCommander`_ object
        # Interfaces to ur5 robot joints
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        # Create a `DisplayTrajectory` publisher
        # Enables the visualization of trajectories in RViz
        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )

        # Reference frame name
        planning_frame = move_group.get_planning_frame()
        print("============ Planning frame: %s" % planning_frame)

        # End effector link name
        eef_link = move_group.get_end_effector_link()
        print("============ End effector link: %s" % eef_link)

        # Lists all planning groups for the robot
        group_names = robot.get_group_names()
        print("============ Available Planning Groups:", robot.get_group_names())

        # Prints the entire state of the robot
        print("============ Printing robot state")
        print(robot.get_current_state())
        print("")

        # Other variables - not all are needed at this time
        self.box_name = ""
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names

    def go_to_joint_state_A(self):
        move_group = self.move_group

        # We want to move the joints into a favorable position so as not to result in a
        # singularity when moving to pose goals or moving through trajectories.
        # We get the joint values from the group and change some of them:
        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = 0
        joint_goal[1] = - 3*(tau / 8)
        joint_goal[2] = tau / 4
        joint_goal[3] = 0
        joint_goal[4] = 0
        joint_goal[5] = tau / 2

        # The go command can be called with joint values, poses, or without any
        # parameters if you have already set the pose or joint target for the group
        move_group.go(joint_goal, wait=True)

        # Calling ``stop()`` prevents residual movement
        move_group.stop()

        # For testing:
        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)

    def plan_cartesian_path_A(self, scale=1):
        
        move_group = self.move_group

        # A set of Cartesian points will be set directly to create a
        # trajectory in the shape of the letter A.
        # Since this is done remotely through a Python script, scale = 1.
        waypoints = []

        wpose = move_group.get_current_pose().pose
        wpose.position.z += scale * 0.2  # move up (z)
        wpose.position.x += scale * 0.2  # and sideways (x)
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.2  # move down (z)
        wpose.position.x += scale * 0.2  # and sideways (x)
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z += scale * 0.1  # move up (z)
        wpose.position.x -= scale * 0.1  # and sideways (x)
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.x -= scale * 0.1  # move sideways (x)
        waypoints.append(copy.deepcopy(wpose))

        # The sum total of these Cartesian points results in the
        # desired trajectory.

        # The path will be generated at 1 cm intervals, hence step = 0.01.  
        # We will disable the jump threshold by setting it to 0.0,
        # ignoring the check for infeasible jumps in joint space, which is sufficient
        # for this tutorial.
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold

        # This is just the planning stage, no movement yet.
        return plan, fraction

    def display_trajectory(self, plan):
    
        robot = self.robot
        display_trajectory_publisher = self.display_trajectory_publisher

        # This function asks RViz to display the desired trajectory.
        # This is not useful at the moment, so it will not be called.

        # A `DisplayTrajectory`_ msg has two primary fields, trajectory_start and trajectory.
        # trajectory_start is the current state of the robot.
        display_trajectory = moveit_msgs.msg.DisplayTrajectory()
        display_trajectory.trajectory_start = robot.get_current_state()
        display_trajectory.trajectory.append(plan)
        # Publish
        display_trajectory_publisher.publish(display_trajectory)

    def execute_plan(self, plan):
        
        move_group = self.move_group

        # execute will follow the previouslty calculaed plan.
        move_group.execute(plan, wait=True)

        # The robot's current joint state must be relatively near to the
        # first waypoint, or the program will fail.

    def go_to_joint_state_B(self):
        move_group = self.move_group

        # We want to move the joints into a favorable position so as not to result in a
        # singularity when moving to pose goals or moving through trajectories.
        # We get the joint values from the group and change some of them:
        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = 0
        joint_goal[1] = - 3*(tau / 8)
        joint_goal[2] = tau / 4
        joint_goal[3] = 0
        joint_goal[4] = 0
        joint_goal[5] = tau / 2

        # The go command can be called with joint values, poses, or without any
        # parameters if you have already set the pose or joint target for the group
        move_group.go(joint_goal, wait=True)

        # Calling ``stop()`` prevents residual movement
        move_group.stop()

        # For testing:
        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)
    
    def plan_cartesian_path_B(self, scale=1):
        
        move_group = self.move_group

        # A set of Cartesian points will be set directly to create a
        # trajectory in the shape of the letter B.
        # Since this is done remotely through a Python script, scale = 1.
        waypoints = []

        wpose = move_group.get_current_pose().pose
        wpose.position.z -= scale * 0.1  # move down (z)
        wpose.position.x += scale * 0.2  # and sideways (x)
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.1  # move down (z)
        wpose.position.x -= scale * 0.2  # and sideways (x)
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.1  # move up (z)
        wpose.position.x += scale * 0.2  # and sideways (x)
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.1  # move up (z)
        wpose.position.x -= scale * 0.2  # and sideways (x)
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z += scale * 0.4  # move up (z)
        waypoints.append(copy.deepcopy(wpose))

        # The sum total of these Cartesian points results in the
        # desired trajectory.

        # The path will be generated at 1 cm intervals, hence step = 0.01.  
        # We will disable the jump threshold by setting it to 0.0,
        # ignoring the check for infeasible jumps in joint space, which is sufficient
        # for this tutorial.
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold

        # This is just the planning stage, no movement yet.
        return plan, fraction

    def go_to_joint_state_P(self):
        move_group = self.move_group

        # We want to move the joints into a favorable position so as not to result in a
        # singularity when moving to pose goals or moving through trajectories.
        # We get the joint values from the group and change some of them:
        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = 0
        joint_goal[1] = - 3*(tau / 8)
        joint_goal[2] = tau / 4
        joint_goal[3] = 0
        joint_goal[4] = 0
        joint_goal[5] = tau / 2

        # The go command can be called with joint values, poses, or without any
        # parameters if you have already set the pose or joint target for the group
        move_group.go(joint_goal, wait=True)

        # Calling ``stop()`` prevents residual movement
        move_group.stop()

        # For testing:
        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)
    
    def plan_cartesian_path_P(self, scale=1):
        
        move_group = self.move_group

        # A set of Cartesian points will be set directly to create a
        # trajectory in the shape of the letter P.
        # Since this is done remotely through a Python script, scale = 1.
        waypoints = []

        wpose = move_group.get_current_pose().pose
        wpose.position.z -= scale * 0.1  # move down (z)
        wpose.position.x += scale * 0.2  # and sideways (x)
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.1  # move down (z)
        wpose.position.x -= scale * 0.2  # and sideways (x)
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z += scale * 0.2  # move down (z)
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.4  # move down (z)
        waypoints.append(copy.deepcopy(wpose))

        # The sum total of these Cartesian points results in the
        # desired trajectory.

        # The path will be generated at 1 cm intervals, hence step = 0.01.  
        # We will disable the jump threshold by setting it to 0.0,
        # ignoring the check for infeasible jumps in joint space, which is sufficient
        # for this tutorial.
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold

        # This is just the planning stage, no movement yet.
        return plan, fraction
    
def main():
    try:
        print("")
        print("----------------------------------------------------------")
        print("Hello! I am Allen Biju Parayil, and we are tracing my initials.")
        print("----------------------------------------------------------")
        print("Press Ctrl-D to exit at any time")
        print("")
        input(
            "============ Press `Enter` to begin tetting up the moveit_commander ..."
        )
        ABP = initialWriter()

        input("============ Press `Enter` to set up and trace the letter A ...")
        ABP.go_to_joint_state_A()
        cartesian_plan, fraction = ABP.plan_cartesian_path_A()
        ABP.display_trajectory(cartesian_plan)
        ABP.execute_plan(cartesian_plan)
        
        input("============ Press `Enter` to reset and trace the letter B ...")
        ABP.go_to_joint_state_B()
        cartesian_plan, fraction = ABP.plan_cartesian_path_B()
        ABP.display_trajectory(cartesian_plan)
        ABP.execute_plan(cartesian_plan)

        input("============ Press `Enter` to reset and trace the letter P ...")
        ABP.go_to_joint_state_P()
        cartesian_plan, fraction = ABP.plan_cartesian_path_P()
        ABP.display_trajectory(cartesian_plan)
        ABP.execute_plan(cartesian_plan)
        
        print("============ Tracing complete!")
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return


if __name__ == "__main__":
    main()
